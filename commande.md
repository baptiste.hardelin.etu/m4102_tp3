<table>
  <tr>
    <td>URI</td>
    <td>Opération</td>
    <td>MIME</td>
    <td>Requête</td>
    <td>Réponse</td>
  </tr>
  <tr>
    <td>/commande</td>
    <td>GET</td>
    <td>
    <-application/json 
    <-application/xml
    </td>
    <td></td>
    <td>liste des commandes (I2)</td>
  </tr>

  <tr>
    <td>/commande/{id}</td>
    <td>GET</td>
    <td>
    <-application/json 
    <-application/xml
    </td>
    <td></td>
    <td>une commande (I2) ou 404</td>
  </tr>

  <tr>
    <td>/commande/{id}/name</td>
    <td>GET</td>
    <td>
    <-text/plain
    </td>
    <td></td>
    <td>le nom de la commande ou 404</td>
  </tr>

  <tr>
    <td>/commande</td>
    <td>POST</td>
    <td>
    <-/->application/json->application/x-www-form-urlencoded
    </td>
    <td>Ingrédient (I1)</td>
    <td>Nouvelle commande (I2) 409 si la commande existe déjà (même nom)</td>
  </tr>

  <tr>
    <td>/commande/{id}/name</td>
    <td>PUT</td>
    <td>
    ->text/plain
    </td>
    <td></td>
    <td>Met à jour le nom de la commande, renvoie une commande (I1)</td>
  </tr>

  <tr>
    <td>/commande/{id}</td>
    <td>DELETE</td>
    <td></td>
    <td></td>
    <td>Supprime une commande</td>
  </tr>

</table>

<h1>Commandes I1</h1>

```JSON

{
  "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
  "name": "Baptiste",
  "pizzas" : ["Regina", "Savoyarde"]
}


```

<h1>Commandes I2</h1>

```JSON

{ "name": "Baptiste", "pizzas" : ["Regina", "Savoyarde"]}

```
