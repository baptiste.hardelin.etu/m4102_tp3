package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;

	public Pizza(String name, List<Ingredient> ingredients) {
		this.ingredients = ingredients;
		this.name = name;
	}

	public Pizza(String name) {
		this(name, new ArrayList<>());
	}

	public Pizza() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + ", ingredients=" + ingredients + "]";
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto res = new PizzaDto();
		res.setName(p.getName());
		res.setIngredients(p.getIngredients());
		res.setId(p.getId());
		return res;
	}

	public static Pizza fromDto(PizzaDto pDto) {
		Pizza res = new Pizza();
		res.setName(pDto.getName());
		res.setIngredients(pDto.getIngredients());
		res.setId(pDto.getId());
		return res;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto pizzaCreateDto) {
		Pizza res = new Pizza();
		res.setName(pizzaCreateDto.getName());
		res.setIngredients(pizzaCreateDto.getIngredients());
		return res;
	}

	public static PizzaCreateDto toPizzaCreateDto(Pizza p) {
		PizzaCreateDto res = new PizzaCreateDto();
		res.setName(p.getName());
		res.setIngredients(p.getIngredients());
		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
