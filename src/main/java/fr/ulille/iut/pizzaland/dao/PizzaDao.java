package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas ("
  		+ "id VARCHAR(128) PRIMARY KEY,"
  		+ "name VARCHAR UNIQUE NOT NULL"
  		+ ")")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ("
  		+ "pizzaId VARCHAR(128),"
  		+ "ingredientID VARCHAR(128),"
  		+ "FOREIGN KEY (pizzaId) REFERENCES Pizzas(id),"
  		+ "FOREIGN KEY (ingredientId) REFERENCES ingredients(id)"
  		+ ")")
  void createAssociationTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropPizzaTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropPizzaIngredientsAssociationTable();
  
  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }
  
  @Transaction
  default void dropTableAndIngredientAssociation() {
	  dropPizzaIngredientsAssociationTable();
	  dropPizzaTable();
  }

  @Transaction
  default void insertPizzawithIngridients(Pizza p){
	  insertPizza(p);
	  IngredientDao IDao = BDDFactory.buildDao(IngredientDao.class);
	  for(Ingredient i:p.getIngredients()) {
		  IDao.insert(i);
		  insertAssos(p.getId(), i.getId());
	  }
  }
  
  @Transaction 
  default Pizza findFullPizzaById(UUID id) {
	  Pizza res=findById(id);
	  res.setIngredients(getAssociedIngredientById(id));
	  return res;
  }


  
  @SqlQuery("SELECT * FROM Pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll();

  @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findById(@Bind("id") UUID id);


  @SqlQuery("SELECT * FROM Pizzas WHERE name= :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(@Bind("name") String name);

  @SqlQuery("SELECT * FROM PizzaIngredientsAssociation AS pa INNER JOIN Ingredients AS i ON i.id = pa.ingredientID WHERE pa.pizzaId = :id ")
  @RegisterBeanMapper(Ingredient.class)
  List<Ingredient> getAssociedIngredientById(@Bind("id") UUID id);
  
  @SqlUpdate("INSERT INTO Pizzas (id,name) Values (:id,:name)")
  void insertPizza(@BindBean Pizza p);
  

  @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (pizzaId,ingredientID) VALUES (:idPizza,:idIngre)")
  void insertAssos(@Bind("idPizza") UUID idPizza,@Bind("idIngre") UUID idIngre);
  
  @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
  void deletePizzaById(@Bind("id")UUID id);
}

