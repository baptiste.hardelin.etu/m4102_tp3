package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaRessource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());

	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	public PizzaRessource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTableAndIngredientAssociation();
	}

	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzasResource:getAll");

		List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json" })
	public PizzaDto getPizzabyId(@PathParam("id") UUID id) {
		try {

			Pizza res = pizzas.findById(id);
			return Pizza.toDto(res);

		} catch (Exception e) {

			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	@Produces({ "application/json", "application/xml" })
    public Response createPizza(PizzaCreateDto pizzaCreateDto) {
        Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        try {
            Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
            pizzas.insertPizzawithIngridients(pizza);
            PizzaDto pizzaDto = Pizza.toDto(pizza);
            URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();
            
            return Response.created(uri).entity(pizzaDto).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }

    }

	@DELETE
	@Path("{id}")
	public Response deleteIngredient(@PathParam("id") UUID id) {
		if (pizzas.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.deletePizzaById(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

}
