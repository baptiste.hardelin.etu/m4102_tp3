package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;

public class PizzasRessourceTest extends JerseyTest {
	private PizzaDao dao;
    private IngredientDao Idao;


    private Pizza p = new Pizza("Calezon");
    private Pizza pWingridient = new Pizza("Calezon",Arrays.asList(new Ingredient[]{new Ingredient("tomate"),new Ingredient("Olive"),new Ingredient("yass")}));
	
    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();
        return new ApiV1();
    }
    
    @Before
    public void setEnvUp() {
      dao = BDDFactory.buildDao(PizzaDao.class);
      dao.createTableAndIngredientAssociation();
      Idao = BDDFactory.buildDao(IngredientDao.class);
      Idao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
       dao.dropTableAndIngredientAssociation();
       Idao.dropTable();
    }
    
    @Test
    public void testGetEmptyPizzaList() {
        Response response = target("/pizzas").request().get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });
        assertEquals(0, pizzas.size());
    }
    
    @Test
    public void getExistingPizzaById(){
      dao.insertPizza(p);
      Response res = target("/pizzas").path(p.getId().toString()).request().get();
      assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());

      assertEquals(p, res.readEntity(Pizza.class));
    }
    
    @Test
    public void getExistingPizzaWithIngredienntById() {
    	dao.insertPizzawithIngridients(pWingridient);
        Response res = target("/pizzas").path(pWingridient.getId().toString()).request().get();
        assertEquals(Response.Status.OK.getStatusCode(), res.getStatus());
        assertEquals(pWingridient, res.readEntity(Pizza.class));
    }

    @Test
    public void getNotExistingPizza(){
      dao.insertPizza(p);
      Response res = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(), res.getStatus());
    }
    
    @Test
    public void testCreatePizza() {

        Response response = target("/pizzas").request().post(Entity.json(Pizza.toPizzaCreateDto(p)));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), p.getName());
    }
    
    @Test
    public void deletePizzaTest() {
        dao.insertPizzawithIngridients(p);

        Response response = target("/pizzas/").path(p.getId().toString()).request().delete();

        assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

        Pizza result = dao.findById(p.getId());
        assertNull(result);
    }
}
